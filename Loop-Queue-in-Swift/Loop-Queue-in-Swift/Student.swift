//
//  Student.swift
//  Loop-Queue-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class Student: NSObject {
    private var no: Int = 0
    private var age: Int = 0
    
    init(no: Int, age: Int) {
        self.no = no
        self.age = age
    }
    
    override var description: String {
        return "[no: \(no), age: \(age)]"
    }
}
