//
//  main.swift
//  Loop-Queue-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Foundation

func testQueue() {
    let q = LoopQueue<Int>(queueCapacity: 5)
    
    var res = q.enQueue(2)
    res = q.enQueue(5)
    
    q.queueTraverse()
    
    let temp = q.deQueue()
    if temp.0 {
        print(temp.1!)
    }
    
    q.queueTraverse()
    
    res = q.enQueue(7)
    res = q.enQueue(12)
    res = q.enQueue(19)
    res = q.enQueue(31)
    res = q.enQueue(50)
    
    if res {
        q.queueTraverse()
    }
    
    q.clearQueue()
    
    q.queueTraverse()
}

func testObjectQueue() {
    let q = LoopQueue<Student>(queueCapacity: 5)
    
    var res = q.enQueue(Student(no: 2, age: 2))
    res = q.enQueue(Student(no: 5, age: 5))
    
    q.queueTraverse()
    
    let temp = q.deQueue()
    if temp.0 {
        print(temp.1!)
    }
    
    q.queueTraverse()
    
    res = q.enQueue(Student(no: 7, age: 7))
    res = q.enQueue(Student(no: 12, age: 12))
    res = q.enQueue(Student(no: 17, age: 19))
    res = q.enQueue(Student(no: 31, age: 31))
    res = q.enQueue(Student(no: 50, age: 50))
    
    if res {
        q.queueTraverse()
    }
    
    q.clearQueue()
    
    q.queueTraverse()
}

print("testQueue")
testQueue()
print("testObjectQueue")
testObjectQueue()
