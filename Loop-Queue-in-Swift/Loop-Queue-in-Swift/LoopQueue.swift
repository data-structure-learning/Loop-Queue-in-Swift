//
//  LoopQueue.swift
//  Loop-Queue-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class LoopQueue<T>: NSObject {
    
    private var m_pQueue: [T] = []
    private var m_iQueueLen: Int = 0
    private var m_iQueueCapacity: Int = 0
    
    private var m_iHead: Int = 0
    private var m_iTail: Int = 0
    
    init(queueCapacity: Int) {
        m_iQueueCapacity = queueCapacity
    }
    
    func clearQueue() {
        m_iQueueLen = 0
        
        m_iHead = 0
        m_iTail = 0
    }
    
    func isEmpty() -> Bool {
        return m_iQueueLen == 0
    }
    
    func isFull() -> Bool {
        return m_iQueueLen == m_iQueueCapacity
    }
    
    func queueLen() -> Int {
        return m_iQueueLen
    }
    
    func enQueue(_ element: T) -> Bool {
        if isFull() {
            return false
        }

        m_pQueue.append(element)
        m_iTail += 1
        m_iTail %= m_iQueueCapacity
        
        m_iQueueLen += 1
        return true
    }
    
    func deQueue() -> (Bool, T?) {
        if isFull() {
            return (false, nil)
        }
        
        let e = m_pQueue[m_iHead]
        m_iHead += 1
        m_iHead %= m_iQueueCapacity
        
        m_iQueueLen -= 1
        return (true, e)
    }
    
    func queueTraverse() {
        for i in m_iHead..<m_iHead+m_iQueueLen {
            print(m_pQueue[i%m_iQueueLen], separator: "", terminator: " ")
        }
        print("")
    }
}
